/**
 * Buffer
 * 
 * Autor: Bruno Russi Lautenschlager
 * Ultima modificacao: 12/10/2017
 */
package exemplo5;

public interface Buffer {

    public void set (int value )throws InterruptedException;
    
    public int get () throws InterruptedException;
    
}
